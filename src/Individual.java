public class Individual {

    private double[] chromosome;

    public Individual(double[] chromosome) {
        this.chromosome = chromosome;
    }

    public double individualEvaluation() {
        double sum = 0;
        for (int i = 0; i < chromosome.length; i++) {
            sum += ((i + 1) * (chromosome[i] * chromosome[i]));
        }
        return sum;
    }

    public double[] getChromosome() {
        return chromosome;
    }
}
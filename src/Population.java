import java.util.*;

public class Population {

    private ArrayList<Individual> individuals = new ArrayList<>();
    private ArrayList<Individual> newIndividuals = new ArrayList<>();
    private double minPopulation = 0.0;

    public Population(ArrayList<Individual> individuals) {
        this.individuals = individuals;
    }

    public void printPopulation() {
        for (int i = 0; i < 300; i++) {
            for (int j = 0; j < 100; j++) {
                System.out.print(individuals.get(i).getChromosome()[j] + "\t\t\t   ");
            }
            System.out.println();
        }
    }

    public double maxPopulation() {
        double max = individuals.get(0).individualEvaluation();
        for (int i = 1; i < individuals.size(); i++) {
            if (individuals.get(i).individualEvaluation() > max) {
                max = individuals.get(i).individualEvaluation();
            }
        }
        return max;
    }

    public double minPopulation() {
        double min = individuals.get(0).individualEvaluation();
        for (int i = 0; i < individuals.size(); i++) {
            if (individuals.get(i).individualEvaluation() < min) {
                min = individuals.get(i).individualEvaluation();
            }
        }

        return min;
    }

    public double averagePopulation() {
        double average = 0.0;
        for (int i = 0; i < individuals.size(); i++) {
            average = average + individuals.get(i).individualEvaluation();
        }
        average = average / individuals.size();
        return average;
    }

    // Roulette Wheel Selection
    public void rouletteSelectionProcess(int sizeOfSelection) {
        // Get the sum of all the individuals
        double sum = 0.0;
        for (int i = 0; i < individuals.size(); i++) {
            sum = sum + individuals.get(i).individualEvaluation();
        }

        // Give every individual a probability to be chosen
        double[] probability = new double[individuals.size()];
        double sumOfProbability = 0.0;
        for (int i = 0; i < individuals.size(); i++) {
            // Inverse from Maximum Function to Minimum Function in order for minimum individual evaluation to be selected more often
            probability[i] = sumOfProbability + (individuals.get(i).individualEvaluation() / sum);
            sumOfProbability = sumOfProbability + probability[i];
        }

        Random random = new Random();
        Individual[] individual = new Individual[sizeOfSelection];
        int count = 0;
        // Log the max and min because the probability values are between 0.0001 to 10000.0
        double logMax = Math.log(probability[299]);
        double logMin = Math.log(probability[0]);

        // Selecting parents (individual) based on their probability given earlier
        while (newIndividuals.size() <= 300) {
            while (count < individual.length) {
                double chance = Math.exp((random.nextDouble() * (logMax - logMin) + logMin));
                for (int i = 0; i < individuals.size(); i++) {
                    if (chance > probability[i]) {
                        individual[count] = individuals.get(i);
                    }
                }
                count++;
            }

            // Sort the Individuals
            Arrays.sort(individual, new Comparator<Individual>() {
                @Override
                public int compare(Individual individual1, Individual individual2) {
                    if (individual1.individualEvaluation() < individual2.individualEvaluation()) {
                        return -1;
                    } else if (individual1.individualEvaluation() == individual2.individualEvaluation()) {
                        return 0;
                    } else {
                        return 1;
                    }
                }
            });

            // Crossover the 2 selected parents (individuals)
            crossoverProcess(individual[0], individual[1]);
            count = 0;
        }

        // Start Mutation process
        mutationProcess();
        individuals.clear();
        individuals.addAll(newIndividuals);
        newIndividuals.clear();
    }

    // Tournament Selection Process
    public void tournamentSelectionProcess(int sizeOfSelection) {
        while (newIndividuals.size() <= 300) {
            Individual parent1 = selectParent(sizeOfSelection);
            Individual parent2 = selectParent(sizeOfSelection);
            crossoverProcess(parent1, parent2);
        }

        // Start Mutation process
        mutationProcess();
        individuals.clear();
        individuals.addAll(newIndividuals);
        newIndividuals.clear();
    }

    // Select Parent for Tournament Selection
    public Individual selectParent(int size) {
        ArrayList<Individual> temp_individuals = new ArrayList<>();
        Random random = new Random();
        int chromosome_index = random.nextInt(300);
        temp_individuals.add(individuals.get(chromosome_index));
        Individual parent = temp_individuals.get(0);

        for (int i = 0; i < size; i++) {
            if (temp_individuals.get(i).individualEvaluation() <= parent.individualEvaluation()){
                parent = temp_individuals.get(i);
            }
            chromosome_index = random.nextInt(300);
            temp_individuals.add(individuals.get(chromosome_index));
        }
        return parent;
    }

    // Single Point Crossover (Random Mid Point)
    public void crossoverProcess(Individual individual1, Individual individual2) {
        Random random = new Random();
        ArrayList<Individual> chosenIndividuals = new ArrayList<>();

        // TODO: Requires code refractoring
        // First Child
        double child1[] = new double[100];
        double chance = random.nextDouble();
        int midpoint = random.nextInt(100);
        if (chance <= 0.7) { // 70% Chance
            for (int i = 0; i <= midpoint; i++) {
                child1[i] = individual1.getChromosome()[i];
            }
            for (int i = midpoint; i < 100; i++) {
                child1[i] = individual2.getChromosome()[i];
            }
            chosenIndividuals.add(new Individual(child1));
            chosenIndividuals.add(individual1);
        } else { // 30% Chance
            chosenIndividuals.add(individual1);
        }

        // Second Child
        double child2[] = new double[100];
        chance = random.nextDouble();
        midpoint = random.nextInt(100);
        if (chance <= 0.7) { // 70% Chance
            for (int i = 0; i <= midpoint; i++) {
                child2[i] = individual2.getChromosome()[i];
            }
            for (int i = midpoint; i < 100; i++) {
                child2[i] = individual1.getChromosome()[i];
            }
            chosenIndividuals.add(new Individual(child2));
            chosenIndividuals.add(individual2);
        } else { // 30% Chance
            chosenIndividuals.add(individual2);
        }

        // Select 2 out of (2 - 3 - 4 Individuals)
        selectBestAfterCrossover(chosenIndividuals);
    }

    // Mutation
    public void mutationProcess() {
        double max = 5.12;
        double min = -5.12;
        Random random = new Random();

        for (int i = 0; i < newIndividuals.size(); i++) {
            if (random.nextDouble() <= 0.1) {
                newIndividuals.get(i).getChromosome()[random.nextInt(100)] = (random.nextDouble() * (max - min)) + min;
            }
        }
    }

    // Select 2 Best Individuals after Crossover Process is done but Before Mutation
    public void selectBestAfterCrossover(ArrayList<Individual> chosenIndividuals) {

        Collections.sort((List) chosenIndividuals, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                Individual individual1 = (Individual) o1;
                Individual individual2 = (Individual) o2;

                if (individual1.individualEvaluation() < individual2.individualEvaluation()) {
                    return -1;
                } else if (individual1.individualEvaluation() == individual2.individualEvaluation()) {
                    return 0;
                } else {
                    return 1;
                }
            }
        });

        newIndividuals.add(chosenIndividuals.get(0));
        newIndividuals.add(chosenIndividuals.get(1));
    }
}
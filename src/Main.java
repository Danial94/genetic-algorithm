import java.util.ArrayList;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        double[][] array = new double[300][100];
        double max = 5.12;
        double min = -5.12;
        double populationSize = 300;

        Random random = new Random();

        for (int i = 0; i < 300; i++) {
            for (int j = 0; j < 100; j++) {
                array[i][j] = (random.nextDouble() * (max - min)) + min;
            }
        }

        ArrayList<Individual> individuals = new ArrayList<>();

        for (int i = 0; i < populationSize; i++) {
            individuals.add(new Individual(array[i]));
        }

        Population population = new Population(individuals);

        System.out.println(1 + " Generation");
        System.out.println("Maximum: " + population.maxPopulation());
        System.out.println("Minimum: " + population.minPopulation());
        System.out.println("Average: " + population.averagePopulation());
        System.out.println();

        for (int i = 1; i < 300; i++) {
            // Just removed one of the commented codes (but comment the other method) to use which selection method you want to use...
            population.rouletteSelectionProcess(2);
            // population.tournamentSelectionProcess(2);

            System.out.println((i + 1) + " Generation");
            System.out.println("Maximum: " + population.maxPopulation());
            System.out.println("Minimum: " + population.minPopulation());
            System.out.println("Average: " + population.averagePopulation());
            System.out.println();
            // population.printPopulation();
        }
    }
}